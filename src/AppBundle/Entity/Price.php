<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use JMS\Serializer\Annotation as JMS;

/**
 * Price
 *
 * @ORM\Table(name="price")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PriceRepository")
 */
class Price
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var int
     *
     * @ORM\Column(name="postal_code_id", type="integer")
     */
    private $postalCodeId;

    /**
     * @var double
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=3)
     */
    private $amount;

    /**
     * @var Date
     * @ORM\Column(name="date", type="date")
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    private $date;

    public function getDateInString()
    {
        return $this->date->format('Y-m-d');
    }
}

