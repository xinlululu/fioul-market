<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class DefaultController extends FOSRestController
{
    /**
     * @Rest\Get("price/{postalCodeId}/{start}/{end}")
     * @ParamConverter("start", options={"format": "Y-m-d"})
     * @ParamConverter("end", options={"format": "Y-m-d"})
     */
    public function indexAction(Request $request, $postalCodeId, \DateTime $start, \DateTime $end)
    {
        $prices = $this->getDoctrine()->getRepository('AppBundle:Price')->getPriceByDate($postalCodeId, $start, $end);
        if (is_null($prices)) {
            return new View("There is no price exist for your demand", Response::HTTP_NOT_FOUND);
        }
        return $prices;
    }
}
