<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class ImportCommand extends ContainerAwareCommand
{
    const DELIMITER = ',';
    const ENCLOSURE = '"';
    const ENDOFLINE = '\n';

    protected function configure()
    {
        $this
            ->setName('import')
            ->setDescription('Import CSV file to database')
            ->addArgument('file', InputArgument::REQUIRED, 'The full file path to import')
            ->addArgument('delimiter', InputArgument::OPTIONAL, 'Delimiter in csv file', static::DELIMITER)
            ->addArgument('enclosure', InputArgument::OPTIONAL, 'Enclosure in csv file', static::ENCLOSURE)
            ->addArgument('endofline', InputArgument::OPTIONAL, 'End of line in csv file', static::ENDOFLINE);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');
        $delimiter = $input->getArgument('delimiter');
        $enclosure = $input->getArgument('enclosure');
        $endofline = $input->getArgument('endofline');

        if (!file_exists($file)) {
            throw new FileNotFoundException();
        }

        $container = $this->getContainer();
        $dbhost = $container->getParameter('database_host');
        $dbuser = $container->getParameter('database_user');
        $dbpass = $container->getParameter('database_password');
        $dbname = $container->getParameter('database_name');
        $dbport = $container->getParameter('database_port');

        $connection = new \PDO("mysql:host=$dbhost;dbname=$dbname;port=$dbport", $dbuser, $dbpass,
            [\PDO::MYSQL_ATTR_LOCAL_INFILE => true, \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]);

        $statement = $connection->prepare('LOAD DATA INFILE :file INTO TABLE price
        FIELDS TERMINATED BY :delimiter
        ENCLOSED BY :enclosure
        LINES TERMINATED BY :endofline
        IGNORE 1 LINES
        (postal_code_id, amount, `date`)');
        $statement->execute(
            [
                'file' => $file,
                'delimiter' => $delimiter,
                'enclosure' => $enclosure,
                'endofline' => $endofline]);

        $output->writeln($connection->lastInsertId() .' lines inserted');
    }

}
