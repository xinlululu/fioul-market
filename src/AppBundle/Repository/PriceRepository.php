<?php

namespace AppBundle\Repository;

/**
 * PriceRepository
 *
 */
class PriceRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param int $postalCodeId
     * @param \DateTime $start
     * @param \DateTime $end
     * @return array
     */
    public function getPriceByDate($postalCodeId, \DateTime $start, \DateTime $end)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT p.date, p.amount
              FROM AppBundle:Price p 
              WHERE p.postalCodeId = :postalCodeId 
              AND p.date BETWEEN :start AND :end')
            ->setParameter('postalCodeId', $postalCodeId)
            ->setParameter('start', $start->format('Y-m-d'))
            ->setParameter('end', $end->format('Y-m-d'));
        return $query->getResult();
    }
}
